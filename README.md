# Stash plugin decorating tutorials

This is the Atlassian Stash example plugin for decorating various pages around Stash.

----

## Project decorator

Add a tab with content to the project page.

![project](http://monosnap.com/image/2fjKPfHnS29rilBsRgXijJBIq.png)

----

## Repository decorator

Add a tab with content to the repository page.

![repo](http://monosnap.com/image/BheKzc2xg5gqOdttSzRIu7wqF.png)

Repository settings page

![repo-settings](http://monosnap.com/image/84y7RaMcD542Jj7iRRenwjPWs.png)

----

## User profile decorator

[User profile tutorial](https://developer.atlassian.com/stash/docs/latest/tutorials-and-examples/decorating-the-user-profile.html) – add content to the user profile page.

![profile](http://monosnap.com/image/UgHGsxIqvla4qAk0QjH0mk4yW.png)

----

## User account decorator

[User account tutorial](https://developer.atlassian.com/stash/docs/latest/tutorials-and-examples/decorating-the-user-account.html) – add content to the user account page.

![account](http://monosnap.com/image/3H2wXG7QHSr39FpnFbDpwnS8A.png)

----

Here are the SDK commands you'll use immediately:

* `atlas-run`   -- installs this plugin into the product and starts it on localhost
* `atlas-debug` -- same as atlas-run, but allows a debugger to attach at port 5005
* `atlas-cli`   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - `pi` reinstalls the plugin into the running product instance
* `atlas-help`  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
